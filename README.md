Here I will record the steps taken to install and run nginx on an AWS machine.


First I logged into the AWS machine. This was using the command:

```
ssh -i ~/.ssh/ch9_shared.pem ec2-user@54.170.88.141
```

with the I.P. address being created and sent by Filipe. I also created a new repository and bash file with the steps to install and run apache. Next I sent the file from the folder where the bash script it to the AWS machine with the command:

```
$ scp -i ~/.ssh/ch9_shared.pem -r bash_script_for_aws ec2-user@54.170.88.141:/home/ec2-user
```

Then I did 

```
chmod +x ./bash_script_for_aws

./bash_script_for_aws
```

which has run the bash script and so Apache is now installed.

Now when I put my new I.P. address into the browser it shows me a test page. It also displays the message 'You may now add content to the directory /var/www/html/' so we can do ```cd ..``` a couple times to find var and go down the recommended path. I have created a html file which I will send to the AWS machine using a similar command. Well actually it wouldn't let me send it to var etc so I sent it to ~. Also, the html file was originally called website.html but this wouldn't do anything once it was in the correct location so I deleted it, renamed it to index.html, moved it again and now it's all gravy :sunglasses: The commands that I used were:

```
scp -i ~/.ssh/ch9_shared.pem index.html ec2-user@54.170.88.141:~/

```

From my home place. Then from the AWS machine:

```
sudo mv index.html /var/www/html/

cd /var/www/html

ls

sudo nano index.html 

```

That last line was just to edit the file a little in order to slightly alter the text. Finally when I refreshed the page that was found by inputting the new I.P. address, it all worked yay.